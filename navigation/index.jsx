import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import TaskList from "../screens/TaskList";
import AddTaskScreen from "../screens/AddTaskScreen";
import ToDoScreen from "../screens/ToDoScreen";

const StackNavigator = createStackNavigator(
  {
    ToDoScreen: {
      screen: ToDoScreen,
    },
    TaskList: {
      screen: TaskList,
    },
    AddTaskScreen: {
      screen: AddTaskScreen,
    },
  },
  {
    initialRouteName: "ToDoScreen",
    headerMode: "none",
    mode: "modal",
  }
);

export default createAppContainer(StackNavigator);
