import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import TaskList from "./TaskList";
import { FAB } from "react-native-paper";

function ToDoScreen({ navigation }) {
  return (
    <LinearGradient style={styles.container} colors={["#DA4453", "#89216B"]}>
      <StatusBar barStyle="light-content" />
      <Text style={styles.appTitle}>La Todo de Goulax</Text>
      <View style={styles.card}>
        <TaskList />
        <FAB
          style={styles.fab}
          large
          icon="plus"
          onPress={() => navigation.navigate("AddTaskScreen")}
        />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  appTitle: {
    color: "#fff",
    fontSize: 36,
    marginTop: 60,
    marginBottom: 30,
    fontWeight: "300",
  },
  card: {
    backgroundColor: "#fff",
    flex: 1,
    width: 300,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  fab: {
    position: "absolute",
    right: 10,
    bottom: 10,
  },
});

export default ToDoScreen;
